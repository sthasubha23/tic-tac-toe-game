import './App.css';
import React, { useState } from 'react';
function App() {
  const [swapX, setSwap] = useState(true);
  const [value, setValue] = useState(Array(9).fill(null));

  const handleClick = (index) => {
    if(value[index]){
      return;
    }
    const newValue = value.slice()
    newValue[index] = swapX ? "X" : "O"
    setValue(newValue)
    setSwap(!swapX)
  };
  const box = (index) => {
    return (
      <button className='btn btn-light m-1' style={{ width: '100px', height: '100px' }} onClick={() => handleClick(index)}>
        {
          value[index] === "X"?(<label className='text-warning'style={{fontSize:'57px'}}>X</label>):
          value[index] === "O"?(<label className='text-danger'style={{fontSize:'57px'}}>O</label>):("")
        }
      </button>
    );
  }

  const check=(box)=>{
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
  
    for (const [a, b, c] of lines) {
      if (box[a] && box[a] === box[b] && box[a] === box[c]) {
        return box[a];
      }
    }
    return null;
  };
  const winner=check(value);

  return (
    <>
      <div className='container-fluid' style={{ backgroundColor: '#212121', height: '100vh' }}>
          <section className='d-flex flex-row justify-content-around'>
            <h4 style={{ color: 'white' }}>
            {
            winner? (`Winner: Player ${winner}`):
            swapX ? ("Status: Your turn X") : ("Status: Your turn O")
          }
          </h4>
          </section>
          <div className='d-flex flex-row justify-content-around'>
          <section >
            <div className="board-row">
              {box(0)}
              {box(1)}
              {box(2)}
            </div>
            <div className="board-row">
              {box(3)}
              {box(4)}
              {box(5)}
            </div>
            <div className="board-row">
              {box(6)}
              {box(7)}
              {box(8)}
            </div>
          </section>
        </div>
      </div>
    </>
  );
}

export default App;
